package com.nx.arch.addon.lock.client;

import com.alibaba.fastjson.JSON;

/**
 * @类名称 EtcdResponse.java
 * @类描述 节点响应对象
 * @作者  庄梦蝶殇 linhuaichuan@naixuejiaoyu.com
 * @创建时间 2020年3月28日 下午3:37:26
 * @版本 1.0.0
 *
 * @修改记录
 * <pre>
 *     版本                       修改人 		修改日期 		 修改内容描述
 *     ----------------------------------------------
 *     1.0.0 		庄梦蝶殇 	2020年3月28日             
 *     ----------------------------------------------
 * </pre>
 */
public class EtcdResponse {
    /**
     * http状态码
     */
    public Integer httpCode = 200;
    
    /**
     * 服务吗
     */
    public Integer serverCode;
    
    public String action;
    
    /**
     * 节点数据
     */
    public EtcdNodeData node;
    
    /**
     * 前任节点数据
     */
    public EtcdNodeData prevNode;
    
    /**
     * 错误码
     */
    public Integer errorCode;
    
    /**
     * 消息
     */
    public String message;
    
    /**
     * 异常原因
     */
    public String cause;
    
    /**
     * 错误索引
     */
    public int errorIndex;
    
    public boolean isError() {
        return errorCode != null;
    }
    
    @Override
    public String toString() {
        return JSON.toJSONString(this);
    }
    
    public String getAction() {
        return action;
    }
    
    public EtcdNodeData getNode() {
        return node;
    }
    
    public EtcdNodeData getPrevNode() {
        return prevNode;
    }
    
    public Integer getErrorCode() {
        return errorCode;
    }
    
    public String getMessage() {
        return message;
    }
    
    public String getCause() {
        return cause;
    }
    
    public int getErrorIndex() {
        return errorIndex;
    }
    
    public Integer getHttpCode() {
        return httpCode;
    }
    
    public EtcdResponse setHttpCode(Integer httpCode) {
        this.httpCode = httpCode;
        return this;
    }
    
    public static EtcdResponse create() {
        return new EtcdResponse();
    }
}
